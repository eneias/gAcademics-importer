# Conversor de Resultados do Google Acedemics



## Sobre este projeto

Este projeto é um script, escrito em linguagem Python (versão 3) que usa um compenente de software de codigo aberto disponivel em https://github.com/eneias/google-scholar para pegar os resultados do site `https://scholar.google.com/`, uma vez que o google não oferece uma API (Application programming interface ou em português Interface de programação de aplicações) para este fim

Este script torna possivel conseguir extrair os resultados do Google Academics e importá-los facilmente em uma planilha


## Como usar

É necessário ter algumas ferramentas de desenvolvimento baixadas:
- Git: https://git-scm.com/downloads
- Python 3: https://www.python.org/downloads/
- Python pip: https://www.dataquest.io/blog/install-pip-windows/

Faça o clone deste repositório.

```
git clone https://gitlab.com/eneias/gAcademics-importer.git
```
ou fala o download dos fontes em https://gitlab.com/eneias/gAcademics-importer/-/archive/main/gAcademics-importer-main.zip

com os arquivos baixados:
- abra o PowerShell do windows ou o terminal do linux
- entre no diretorio onde estão os arquivos
- execute os comandos:
  - pip3 install scholarly (para instalar as dependencias)
  - Execute o script `run.py` conforme mostrado abaixo:

```
python3 run.py -q "palavras chaves a buscar" -o "nome do arquivo que será gerado com os resultados"

```

Apos executar o script, basta pegar o arquivo que foi gerado e importar no excel ou google sheet


# Limitação Técnica

O Google Academics está com um defeito (bug), onde só é possivel ler os primeiros 1000 (mil) resultados.

Para ver isso acontecer, acesse https://scholar.google.com/scholar?start=970&q=programa%C3%A7%C3%A3o+&hl=en&as_sdt=0,33

Esta é a página 98. ao tentar ir para a proxima pagina, vemos o seguinte erro:
```
Server Error
We're sorry but it appears that there has been an internal server error while processing your request. Our engineers have been notified and are working to resolve the issue.
Please try again later.
```