import sys
import argparse
import json
from scholarly import scholarly, ProxyGenerator



import logging
logging.basicConfig(filename='log.log', encoding='utf-8', level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s',datefmt='%d/%m/%Y %H:%M:%S')
logging.debug('\n\n\n\n\n>>>> Inicio dos logs\n\n\n')


def salva_index(numero, cfg_filename):
    with open(cfg_filename, 'w') as arquivo:
        arquivo.write(str(numero))

def le_index(cfg_filename):
    try:
        with open(cfg_filename, 'r') as arquivo:
            numero = int(arquivo.read())
            return numero + 1
    except Exception as e:
        return 0

def saveHeader(filename):
    try:
        text = None
        with open(filename, 'r', encoding='utf-8') as arquivo:
            text = arquivo.read()
        if not text:
            line = "titulo|autor|ano|local|resumo|pub_url|url_scholarbib|url_add_sclib|num_citations|citedby_url|url_related_articles|eprint_url\n"
            with open(filename, "w", encoding='utf-8') as outfile:
                outfile.write(line)
    except Exception as e:
        pass


def normalize(string):
    return string.replace("|", "-")


def save(filename, publication):
    line = "{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}\n".format(
        normalize(publication['bib']['title']),
        ";".join(publication['bib']['author']),
        publication['bib']['pub_year'],
        normalize(publication['bib']['venue']),
        normalize(publication['bib']['abstract']),
        publication.get('pub_url', '-'),
        publication.get('url_scholarbib', '-'),
        publication.get('url_add_sclib', '-'),
        publication.get('num_citations', '-'),
        publication.get('citedby_url', '-'),
        publication.get('url_related_articles', '-'),
        publication.get('eprint_url', '-')
    )
    with open(filename, "a", encoding='utf-8') as outfile:
        outfile.write(line)

def proxy_free():
    pg = ProxyGenerator()
    pg.FreeProxies(timeout=5, wait_time=600)
    scholarly.use_proxy(pg)

def proxy_scraper():
    pg = ProxyGenerator()
    success = pg.ScraperAPI('f20545ee67dd3e78bd0de6582424d205', country_code='us', premium=True, render=True)
    # success = pg.ScraperAPI('f20545ee67dd3e78bd0de6582424d205', premium=True)
    # success = pg.ScraperAPI('603d5e486bc0f5d670d18eb4962ffdf7') #and account
    if not success:
        print ("deu ruim no ScraperAPI")
        logging.debug("[neo] deu ruim no ScraperAPI")
        exit()

def proxy_tor():
    pg = ProxyGenerator()
    # success = pg.Tor_Internal(tor_cmd = "C:\\Tor\\Browser\\TorBrowser\\Tor\\tor.exe")
    success = pg.Tor_External(tor_sock_port=9050, tor_control_port=9051, tor_password="scholarly_password")

    scholarly.use_proxy(pg)

def search_pubsIA(query, output_file):

    try:
        print("https://scholarly.readthedocs.io/en/stable/quickstart.html#using-proxies")
        #proxy_free()
        proxy_scraper()
        # proxy_tor()

    except Exception as e:
        logging.debug("[neo] Exception: " + str(e))
        print(e)

    pagina = int( le_index(output_file+".txt") )

    # Realizar a pesquisa
    search_results = scholarly.search_pubs(query, start_index=pagina)

    # Lista para armazenar as publicações
    publications = []

    # Loop pelos resultados da pesquisa
    saveHeader(output_file)

    for i, result in enumerate(search_results):
        # print(json.dumps( result , indent=4))

        pg_prt = str( int(pagina) + int(i) )

        print( pg_prt, ") ", result['bib']['title'])
        logging.debug("[neo] "+pg_prt+") "+result['bib']['title'])

        salva_index( int(pagina) + int(i), output_file+".txt" )
        if i % 20 == 0:
            pagina = str(i%20)
            print( "--- Pagina: " + str(pagina) )
            logging.debug( "[neo] --- Pagina: " + str(pagina) )
        # print (type(result))

        save(output_file, result)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog=sys.argv[0],
                                     description='Le as publicações do Google Academics')

    parser.add_argument('-q', '--query', action='store', nargs='*', dest='query',
                        help='palavra chave de busca')
    parser.add_argument('-o', '--output', help="arquivo contendo o resultado")
    params = parser.parse_args()

    busca = ""
    arquivo = params.output
    if not arquivo.lower().endswith(".csv"):
        arquivo = arquivo + ".csv"

    if not params.query:
        # busca = '(Ensino OR Educação OR Pedagog*) AND Reabilita* AND ("Deficiência Mental" OR "Deficiências mentais" OR "Deficiência Intelectual") AND (Idoso* OR Idosa* OR Envelhecimento)'
        # busca = '(Enseñanza OR Educación OR Pedagog*) AND Rehabilita* AND ("Discapacidad Mental" OR "Discapacidades Mentales" OR "Discapacidad Intelectual") AND (Anciano* OR Envejecimiento)'
        busca = '(Teaching OR Education OR Pedagog*) AND Rehabilita* AND ("Mental Disabilities" OR "Mental Disability" OR "Intellectually Disabled" OR "Mentally Disabled" OR "Mentally Retarded" OR "Intellectual Disability") AND (Aged OR "elderly" OR Aging)'
    else:
        busca = params.query[0]

    print("Buscando por: " + busca)
    logging.debug("[neo] Buscando por: " + busca)

    print("Arquivo a ser gerado: " + arquivo)
    logging.debug("[neo] Arquivo a ser gerado: " + arquivo)

    search_pubsIA(busca, arquivo)

    print(" >>> done. ")
    logging.debug("[neo]  >>> done. ")
